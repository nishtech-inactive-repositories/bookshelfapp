﻿using Bookshelf.Managers;
using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Bookshelf
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ConfigurationsPage : Page
    {
        private readonly Utils Utils = Utils.Instance;

        /// <summary>
        /// Reference back to the "root" page of the app.
        /// </summary>
        private MainPage RootPage;

        public ConfigurationsPage()
        {
            this.InitializeComponent();
            GetLocalParameters();
        }

        private void GetLocalParameters()
        {
            IDictionary<string, object> listKeys = this.Utils.StoredKeyValue;
            if (listKeys is null)
            {
                this.SaveParameters(false);
                listKeys = this.Utils.StoredKeyValue;
            }

            textCountFramesToShoot.Text = listKeys[Constants.CONFIGURATION_COUNT_FRAMES_TO_SHOOT] == null ? "15" : listKeys[Constants.CONFIGURATION_COUNT_FRAMES_TO_SHOOT].ToString();
            textTimerFaceDetect.Text = listKeys[Constants.CONFIGURATION_TIMER_FACE_DETECT] == null ? "100" : listKeys[Constants.CONFIGURATION_TIMER_FACE_DETECT].ToString();
            textTimerExperience.Text = listKeys[Constants.CONFIGURATION_TIMER_EXPERIENCE] == null ? "10000" : listKeys[Constants.CONFIGURATION_TIMER_EXPERIENCE].ToString();
            textKairosID.Text = listKeys[Constants.CONFIGURATION_KAIROS_APPLICATION_ID] == null ? "d50968b5" : listKeys[Constants.CONFIGURATION_KAIROS_APPLICATION_ID].ToString();
            textKayrosKey.Text = listKeys[Constants.CONFIGURATION_KAIROS_APPLICATION_KEY] == null ? "2e5b58fe6af5b19ea8e4ebcac0e33649" : listKeys[Constants.CONFIGURATION_KAIROS_APPLICATION_KEY].ToString();
            textKayrosGalleryID.Text = listKeys[Constants.CONFIGURATION_KAIROS_GALLERY_KEY] == null ? "SUGCON" : listKeys[Constants.CONFIGURATION_KAIROS_GALLERY_KEY].ToString();
            textSitecoreURL.Text = listKeys[Constants.CONFIGURATION_SITECORE_URL] == null ? "http://bookshelf/recommendedapp" : listKeys[Constants.CONFIGURATION_SITECORE_URL].ToString();
            textSitecoreURL_API.Text = listKeys[Constants.CONFIGURATION_SITECORE_URL_API] == null ? "http://bookshelf/sitecore/api/layout/render/jss?placeholderName=gridcolumn&item={A0498BE5-EA91-44F5-A60A-1E184FFC06C9}&sc_apikey={B347A5E0-EB5D-453B-B3BE-C6D448FB7204}" : listKeys[Constants.CONFIGURATION_SITECORE_URL_API].ToString();
            textIdentifierName.Text = listKeys[Constants.CONFIGURATION_IDENTIFIER_NAME] == null ? "SUGCON" : listKeys[Constants.CONFIGURATION_IDENTIFIER_NAME].ToString();
        }

        private void SaveParameters(bool getFromUI)
        {
            if(getFromUI)
                if (!ValidateUIFields())
                    return;

            IDictionary<string, object> keyValuePairs = new Dictionary<string, object>();
            keyValuePairs.Add(Constants.CONFIGURATION_COUNT_FRAMES_TO_SHOOT, getFromUI ? textCountFramesToShoot.Text : "15");
            keyValuePairs.Add(Constants.CONFIGURATION_TIMER_FACE_DETECT, getFromUI ? textTimerFaceDetect.Text : "100");
            keyValuePairs.Add(Constants.CONFIGURATION_TIMER_EXPERIENCE, getFromUI ? textTimerExperience.Text : "10000");
            keyValuePairs.Add(Constants.CONFIGURATION_KAIROS_APPLICATION_ID, getFromUI ? textKairosID.Text : "d50968b5");
            keyValuePairs.Add(Constants.CONFIGURATION_KAIROS_APPLICATION_KEY, getFromUI ? textKayrosKey.Text : "2e5b58fe6af5b19ea8e4ebcac0e33649");
            keyValuePairs.Add(Constants.CONFIGURATION_KAIROS_GALLERY_KEY, getFromUI ? textKayrosGalleryID.Text : "SUGCON");
            keyValuePairs.Add(Constants.CONFIGURATION_SITECORE_URL, getFromUI ? textSitecoreURL.Text : "http://bookshelf/recommendedapp");
            keyValuePairs.Add(Constants.CONFIGURATION_SITECORE_URL_API, getFromUI ? textSitecoreURL_API.Text : "http://bookshelf/sitecore/api/layout/render/jss?placeholderName=gridcolumn&item={A0498BE5-EA91-44F5-A60A-1E184FFC06C9}&sc_apikey={B347A5E0-EB5D-453B-B3BE-C6D448FB7204}");
            keyValuePairs.Add(Constants.CONFIGURATION_IDENTIFIER_NAME, getFromUI ? textIdentifierName.Text : "SUGCON");

            this.Utils.StoredKeyValue = keyValuePairs;
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            this.SaveParameters(true);
            this.RootPage.NotifyUser("Success", NotifyType.StatusMessage);
        }

        private bool CheckTypeValue(string value)
        {
            int outValue;
            return Int32.TryParse(value, out outValue);
        }

        private bool ValidateUIFields()
        {
            string message = string.Empty;

            if(!CheckTypeValue(textCountFramesToShoot.Text))
                message += Constants.CONFIGURATION_COUNT_FRAMES_TO_SHOOT_MESSAGE;
            if (!CheckTypeValue(textTimerFaceDetect.Text))
                message = (message == string.Empty) ? Constants.CONFIGURATION_TIMER_FACE_DETECT_MESSAGE :
                     (message + Environment.NewLine + Constants.CONFIGURATION_TIMER_FACE_DETECT_MESSAGE);
            if (!CheckTypeValue(textTimerExperience.Text))
                message = (message == string.Empty) ? Constants.CONFIGURATION_TIMER_EXPERIENCE_MESSAGE : 
                    (message + Environment.NewLine + Constants.CONFIGURATION_TIMER_EXPERIENCE_MESSAGE);
            if (string.IsNullOrEmpty(textKairosID.Text))
                message = (message == string.Empty) ? Constants.CONFIGURATION_KAIROS_APPLICATION_ID_MESSAGE :
                    (message + Environment.NewLine + Constants.CONFIGURATION_KAIROS_APPLICATION_ID_MESSAGE);
            if (string.IsNullOrEmpty(textKayrosKey.Text))
                message = (message == string.Empty) ? Constants.CONFIGURATION_KAIROS_APPLICATION_KEY_MESSAGE :
                    (message + Environment.NewLine + Constants.CONFIGURATION_KAIROS_APPLICATION_KEY_MESSAGE);
            if (string.IsNullOrEmpty(textKayrosGalleryID.Text))
                message = (message == string.Empty) ? Constants.CONFIGURATION_KAIROS_GALLERY_KEY_MESSAGE :
                    (message + Environment.NewLine + Constants.CONFIGURATION_KAIROS_GALLERY_KEY_MESSAGE);
            if (string.IsNullOrEmpty(textSitecoreURL.Text))
                message = (message == string.Empty) ? Constants.CONFIGURATION_SITECORE_URL_MESSAGE :
                    (message + Environment.NewLine + Constants.CONFIGURATION_SITECORE_URL_MESSAGE);
            if (string.IsNullOrEmpty(textSitecoreURL_API.Text))
                message = (message == string.Empty) ? Constants.CONFIGURATION_SITECORE_URL_API_MESSAGE :
                    (message + Environment.NewLine + Constants.CONFIGURATION_SITECORE_URL_API_MESSAGE);
            if (string.IsNullOrEmpty(textIdentifierName.Text))
                message = (message == string.Empty) ? Constants.CONFIGURATION_IDENTIFIER_NAME_MESSAGE :
                    (message + Environment.NewLine + Constants.CONFIGURATION_IDENTIFIER_NAME_MESSAGE);

            this.RootPage.NotifyUser(message, NotifyType.ErrorMessage);
            return String.IsNullOrEmpty(message);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.RootPage = MainPage.Current;
        }

        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            this.SaveParameters(false);
            this.RootPage.NotifyUser("Success", NotifyType.StatusMessage);
        }
    }
}
