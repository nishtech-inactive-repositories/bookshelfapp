﻿using Bookshelf.Managers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Imaging;
using Windows.Media.FaceAnalysis;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

namespace Bookshelf
{
    public sealed partial class ManualFaceDetectionPage : Page
    {
        /// <summary>
        /// Brush for drawing the bounding box around each identified face.
        /// </summary>
        private readonly SolidColorBrush lineBrush = new SolidColorBrush(Windows.UI.Colors.Yellow);

        /// <summary>
        /// Thickness of the face bounding box lines.
        /// </summary>
        private readonly double lineThickness = 2.0;

        /// <summary>
        /// Transparent fill for the bounding box.
        /// </summary>
        private readonly SolidColorBrush fillBrush = new SolidColorBrush(Windows.UI.Colors.Transparent);

        private SoftwareBitmap SoftwareBitmap;

        private Utils UtilsSingleton;
        private Managers.Kairos KairosSingleton;
        private StorageFile LocalFile;
        public string KairosUserLogin;
        private CameraFaceDetection camera = new CameraFaceDetection();

        /// <summary>
        /// Reference back to the "root" page of the app.
        /// </summary>
        private MainPage RootPage;
        private bool OpenDetectFaceButton
        {
            set
            {
                buttonDetectFace.IsEnabled = value;
            }
        }
        public ManualFaceDetectionPage()
        {
            this.InitializeComponent();
            this.UtilsSingleton = Utils.Instance;
            this.KairosSingleton = Managers.Kairos.Instance;
            this.textBoxSource.IsReadOnly = true;
            this.OpenDetectFaceButton = false;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.RootPage = MainPage.Current;
        }

        private async void ButtonDetectFace_Click(object sender, RoutedEventArgs e)
        {
            this.RootPage.NotifyUser("", NotifyType.StatusMessage);

            if (this.UtilsSingleton.UseFaceDetection(this.SoftwareBitmap))
            {
                await camera.InitializeFaceDetection();
                var facesDetected = await camera.DetectFaces(this.SoftwareBitmap);
                await SetFaceBox(this.SoftwareBitmap, facesDetected);
            }

            //Regardless find the face, call Kairos, only in Manual Face Detection.
            await CallKairosLogin();
        }

        private async Task CallKairosLogin()
        {
            string base64 = await this.UtilsSingleton.GetBase64FromStorageFile(this.LocalFile);

            var login = this.KairosSingleton.FaceLogin(base64);
            ShowLoginResult(login);
        }

        private void ShowLoginResult(object result)
        {
            var authLogin = JsonConvert.DeserializeObject<AuthLogin>(result.ToString());
            this.RootPage.NotifyUser(authLogin.Message, authLogin.Success ? NotifyType.StatusMessage : NotifyType.ErrorMessage);
            this.KairosUserLogin = authLogin.SubjectID;

            if (authLogin.Success)
                this.Frame.Navigate(typeof(WebBrowserPage), this);
        }

        private async Task SetSoftwareBitmap()
        {
            this.SoftwareBitmap = await camera.GetSoftwareBitmapFromStorageFile(this.LocalFile);
        }

        private async Task SetCanvasToSelectedImage()
        {
            await SetSoftwareBitmap();

            ImageBrush brush = new ImageBrush();
            SoftwareBitmapSource bitmapSource = new SoftwareBitmapSource();
            await bitmapSource.SetBitmapAsync(this.SoftwareBitmap);
            brush.ImageSource = bitmapSource;
            brush.Stretch = Stretch.Fill;
            this.VisualizationCanvas.Background = brush;
            this.VisualizationCanvas.Children.Clear();
        }

        private async Task SetFaceBox(SoftwareBitmap sourceBitmap, IList<DetectedFace> faces)
        {
            await SetCanvasToSelectedImage();

            if (faces.Count == 0)
            {
                this.RootPage.NotifyUser("No faces was detected. Please, select another photo.", NotifyType.ErrorMessage);
                return;
            }
            if (faces.Count > 1)
            {
                this.RootPage.NotifyUser("Please, select a photo with only one face.", NotifyType.ErrorMessage);
                return;
            }

            double widthScale = sourceBitmap.PixelWidth / this.VisualizationCanvas.ActualWidth;
            double heightScale = sourceBitmap.PixelHeight / this.VisualizationCanvas.ActualHeight;

            Rectangle box = new Rectangle();
            box.Width = (uint)(faces[0].FaceBox.Width / widthScale);
            box.Height = (uint)(faces[0].FaceBox.Height / heightScale);
            box.Fill = this.fillBrush;
            box.Stroke = this.lineBrush;
            box.StrokeThickness = this.lineThickness;
            box.Margin = new Thickness((uint)(faces[0].FaceBox.X / widthScale), (uint)(faces[0].FaceBox.Y / heightScale), 0, 0);

            this.VisualizationCanvas.Children.Add(box);
            return;
        }

        private async void ButtonGetLocalImage_Click(object sender, RoutedEventArgs e)
        {
            this.LocalFile = await this.UtilsSingleton.OpenFileDialog_GetLocation();
            textBoxSource.Text = this.LocalFile?.Path;
            this.OpenDetectFaceButton = !string.IsNullOrEmpty(textBoxSource.Text);

            if (buttonDetectFace.IsEnabled)
                await SetCanvasToSelectedImage();

            this.RootPage.NotifyUser("", NotifyType.StatusMessage);
        }
    }
}
