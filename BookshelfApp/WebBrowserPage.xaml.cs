﻿using Bookshelf.Managers;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Windows.System.Threading;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;
using Windows.Web.Http.Filters;

namespace Bookshelf
{
    public sealed partial class WebBrowserPage : Page
    {
        private Utils UtilsSingleton;
        private string KairosSubjectID;
        private object SitecoreURL;
        private object SitecoreURL_API;
        private object IdentifierName;
        private object ClassRoute;
        private MainPage RootPage;
        private GpioControl[] Gpios;
        private ThreadPoolTimer ThreadPoolTimerControl;

        public WebBrowserPage()
        {
            this.InitializeComponent();

            this.UtilsSingleton = Utils.Instance;
            this.SitecoreURL = this.UtilsSingleton.GetParameterFromDataStore(Constants.CONFIGURATION_SITECORE_URL);
            this.SitecoreURL_API = this.UtilsSingleton.GetParameterFromDataStore(Constants.CONFIGURATION_SITECORE_URL_API);
            this.IdentifierName = this.UtilsSingleton.GetParameterFromDataStore(Constants.CONFIGURATION_IDENTIFIER_NAME);

            var intervalTimerExperience = this.UtilsSingleton.GetParameterFromDataStore(Constants.CONFIGURATION_TIMER_EXPERIENCE);
            if (intervalTimerExperience is null)
                intervalTimerExperience = 0;

            Int32.TryParse(intervalTimerExperience.ToString(), out int timerExperience);
            this.ThreadPoolTimerControl = ThreadPoolTimer.CreateTimer(ControlTimerUserExperience, 
                TimeSpan.FromMilliseconds(timerExperience <= 0 ? 60000 : timerExperience)); //Set 60 seconds to default value

            this.webViewBookshelf.NavigationStarting += WebViewBookshelf_NavigationStarting;
        }

        protected override async void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);

            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                this.ThreadPoolTimerControl.Cancel();
                if (this.Gpios != null)
                {
                    foreach (var gpio in this.Gpios)
                    {
                        if (gpio is null)
                            continue;

                        gpio.SendToPin(false);
                    }

                    this.Gpios = null;
                }
            });
        }

        private void WebViewBookshelf_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            Uri gotouri = args.Uri;
            HttpBaseProtocolFilter myFilter = new HttpBaseProtocolFilter();
            HttpCookieManager cookieManager = myFilter.CookieManager;
            HttpCookieCollection myCookieJar = cookieManager.GetCookies(gotouri);

            foreach (HttpCookie cookie in myCookieJar)
            {
                cookieManager.DeleteCookie(cookie);
            }
        }

        private async void ControlTimerUserExperience(ThreadPoolTimer timer)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => {
                this.RootPage.NotifyUser("", NotifyType.StatusMessage);

                if (this.Gpios != null)
                {
                    foreach (var gpio in this.Gpios)
                    {
                        if (gpio is null)
                            continue;

                        gpio.SendToPin(false);
                    }

                    this.Gpios = null;
                }

                this.Frame.Navigate((this.ClassRoute.GetType() == typeof(ManualFaceDetectionPage) ? 
                    typeof(ManualFaceDetectionPage) : typeof(FaceDetection)), true);
            });
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            this.RootPage = MainPage.Current;

            if (e.Parameter is null)
                return;

            SetParameterClassRouteAndKairosLogin(e.Parameter);

            base.OnNavigatedTo(e);

            this.RootPage.NotifyUser("", NotifyType.StatusMessage);

            NavigateToUrl();

            await CallSitecoreApi();
        }

        private void SetParameterClassRouteAndKairosLogin(object parameter)
        {
            if (parameter.GetType() == typeof(ManualFaceDetectionPage))
            {
                this.KairosSubjectID = ((ManualFaceDetectionPage)parameter).KairosUserLogin;
                this.ClassRoute = parameter as ManualFaceDetectionPage;
            }
            else if (parameter.GetType() == typeof(FaceDetection))
            {
                this.KairosSubjectID = ((FaceDetection)parameter).KairosUserLogin;
                this.ClassRoute = parameter as FaceDetection;
            }
        }

        private void NavigateToUrl()
        {
            if (this.SitecoreURL != null && this.IdentifierName != null && !string.IsNullOrEmpty(this.KairosSubjectID))
            {
                Uri uri = new Uri($"{SitecoreURL.ToString()}?d={DateTime.Now.ToString()}");
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, uri);
                request.Headers.Add(Constants.WEBBROWSER_HEADER_NAME, this.IdentifierName.ToString());
                request.Headers.Add(Constants.WEBBROWSER_HEADER_KAIROS, this.KairosSubjectID);

                this.webViewBookshelf.NavigateWithHttpRequestMessage(request);
            }
        }

        private async Task CallSitecoreApi()
        {
            var jsonResponse = await GetHttpSitecoreResponse();
            ProcessJsonReturned(jsonResponse);
        }

        private async Task<string> GetHttpSitecoreResponse()
        {
            if (this.SitecoreURL_API != null && this.IdentifierName != null)
            {
                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(this.SitecoreURL_API.ToString());
                    request.Headers.Add(Constants.WEBBROWSER_HEADER_NAME, this.IdentifierName.ToString());
                    request.Headers.Add(Constants.WEBBROWSER_HEADER_KAIROS, this.KairosSubjectID);
                    request.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                    WebResponse response = await request.GetResponseAsync();
                    using (Stream stream = response.GetResponseStream())
                    {
                        StreamReader sr = new StreamReader(stream);
                        return await sr.ReadToEndAsync();
                    }
                }
                catch (WebException webError)
                {
                    this.RootPage.NotifyUser(webError.Message, NotifyType.ErrorMessage);
                }
                catch (Exception error)
                {
                    this.RootPage.NotifyUser(error.Message, NotifyType.ErrorMessage);
                }
            }

            return string.Empty;
        }

        private void ProcessJsonReturned(string stringJson)
        {
            var jsonConvert = JsonConvert.DeserializeObject<SitecoreJsonResponse>(stringJson);
            if (jsonConvert != null)
            {
                var countResults = GetJsonCountObjects(jsonConvert);
                if (countResults > 0)
                {
                    Gpios = new GpioControl[Convert.ToInt32(countResults)];
                    bool ledBlink = CreateGpiosArray(jsonConvert, Convert.ToInt32(countResults));
                    this.RootPage.NotifyUser(ledBlink ? Constants.WEBBROWSER_MESSAGE_OK : Constants.WEBBROWSER_MESSAGE_NO_SECTIONS, NotifyType.StatusMessage);
                    return;
                }

                this.RootPage.NotifyUser(Constants.WEBBROWSER_MESSAGE_NO_SECTIONS, NotifyType.StatusMessage);
                return;
            }

            this.RootPage.NotifyUser(Constants.WEBBROWSER_ERROR_MESSAGE_SITECORE_RESULTS, NotifyType.ErrorMessage);
        }

        private int GetJsonCountObjects(SitecoreJsonResponse jsonConvert)
        {
            if (jsonConvert?.Sitecore?.Route?.PlaceholdersCompositeBlock?.CompositeBlockSingleColumnContainer != null)
            {
                var compositeBlockSingleColumnContainer = jsonConvert?.Sitecore?.Route?.PlaceholdersCompositeBlock?.CompositeBlockSingleColumnContainer;
                if (compositeBlockSingleColumnContainer[0].PlaceholdersSingleColumnContainer?.GridcolumnSingleColumnContainer != null)
                {
                    var gridcolumnSingleColumnContainer = compositeBlockSingleColumnContainer[0].PlaceholdersSingleColumnContainer?.GridcolumnSingleColumnContainer;
                    if (gridcolumnSingleColumnContainer[0].PlaceholdersGenreBooksContinuous?.GridcolumnGenreBooksContinuous != null)
                    {
                        var gridcolumnGenreBooksContinuous = gridcolumnSingleColumnContainer[0].PlaceholdersGenreBooksContinuous?.GridcolumnGenreBooksContinuous;
                        var count = gridcolumnGenreBooksContinuous.Length;
                        Int32.TryParse(count.ToString(), out int result);
                        return result;
                    }
                }
            }
            return default(int);
        }

        private bool CreateGpiosArray(SitecoreJsonResponse jsonConvert, int arrayLength)
        {
            bool result = false;

            for (int x = 0; x < arrayLength; x++)
            {
                var row = jsonConvert?.Sitecore?.Route?
                    .PlaceholdersCompositeBlock
                    .CompositeBlockSingleColumnContainer[0]
                    .PlaceholdersSingleColumnContainer
                    .GridcolumnSingleColumnContainer[0]
                    .PlaceholdersGenreBooksContinuous
                    .GridcolumnGenreBooksContinuous[x];

                if (row != null)
                {
                    var pinNumber = row.Fields?.PinNumber;
                    if (pinNumber is null)
                        continue;

                    Int32.TryParse(pinNumber.Pin, out int pin);
                    if (pin == 0)
                        continue;

                    Gpios[x] = new GpioControl(pin);

                    //This function returns a bool, that means, 
                    //if False, something was wrong on get Gpio control and if True, when all was Ok.
                    Gpios[x].SendToPin(true);

                    result = true;
                }
            }

            return result;
        }
    }
}
