﻿using Bookshelf.Managers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Graphics.Imaging;
using Windows.Media;
using Windows.Media.Capture;
using Windows.Media.FaceAnalysis;
using Windows.Media.MediaProperties;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

namespace Bookshelf
{
    public sealed partial class FaceDetection : Page
    {
        CameraPreviewManager CameraPreviewManager;
        readonly DevicesSingleton DevicesSingleton;
        readonly Utils UtilsSingleton;
        readonly Managers.Kairos KairosSingleton;
        CameraFaceDetection CameraFaceDetection;
        public string KairosUserLogin;

        /// <summary>
        /// Brush for drawing the bounding box around each identified face.
        /// </summary>
        private readonly SolidColorBrush lineBrush = new SolidColorBrush(Windows.UI.Colors.Yellow);

        /// <summary>
        /// Thickness of the face bounding box lines.
        /// </summary>
        private readonly double lineThickness = 2.0;

        /// <summary>
        /// Transparent fill for the bounding box.
        /// </summary>
        private readonly SolidColorBrush fillBrush = new SolidColorBrush(Windows.UI.Colors.Transparent);

        /// <summary>
        /// Reference back to the "root" page of the app.
        /// </summary>
        private MainPage rootPage;

        /// <summary>
        /// Holds the current scenario state value.
        /// </summary>
        private ScenarioState currentState;

        /// <summary>
        /// References a MediaCapture instance; is null when not in Streaming state.
        /// </summary>
        private MediaCapture mediaCapture;

        /// <summary>
        /// Cache of properties from the current MediaCapture device which is used for capturing the preview frame.
        /// </summary>
        private VideoEncodingProperties videoProperties;

        /// <summary>
        /// References a FaceTracker instance.
        /// </summary>
        private FaceTracker faceTracker;

        /// <summary>
        /// A periodic timer to execute FaceTracker on preview frames
        /// </summary>
        private ThreadPoolTimer frameProcessingTimer;

        /// <summary>
        /// Semaphore to ensure FaceTracking logic only executes one at a time
        /// </summary>
        private SemaphoreSlim frameProcessingSemaphore = new SemaphoreSlim(1);

        /// <summary>
        /// A list of boxes to get best shoot from user.
        /// </summary>
        private IList<Rectangle> Boxes;

        public FaceDetection()
        {
            this.InitializeComponent();

            //Get Singleton instance for find Devices on hardware.
            this.UtilsSingleton = Utils.Instance;
            this.DevicesSingleton = DevicesSingleton.Instance;
            this.KairosSingleton = Managers.Kairos.Instance;

            Window.Current.CoreWindow.CharacterReceived += CoreWindow_CharacterReceived;

            this.currentState = ScenarioState.Idle;
            App.Current.Suspending += this.OnSuspending;

            Boxes = new List<Rectangle>();
        }

        /// <summary>
        /// Responds when we navigate to this page.
        /// </summary>
        /// <param name="e">Event data</param>
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            this.rootPage = MainPage.Current;
            await GetDevicesList();

            // The 'await' operation can only be used from within an async method but class constructors
            // cannot be labeled as async, and so we'll initialize FaceTracker here.
            if (this.faceTracker == null)
            {
                this.faceTracker = await FaceTracker.CreateAsync();
            }

            base.OnNavigatedTo(e);

            if (e.Parameter is null)
                return;

            bool.TryParse(e.Parameter.ToString(), out bool result);

            if (result)
            {
                EnterFullScreenCameraPreview();
                StartStreamingProcess();
            }    
        }

        /// <summary>
        /// Responds to App Suspend event to stop/release MediaCapture object if it's running and return to Idle state.
        /// </summary>
        /// <param name="sender">The source of the Suspending event</param>
        /// <param name="e">Event data</param>
        private void OnSuspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {
            if (this.currentState == ScenarioState.Streaming)
            {
                var deferral = e.SuspendingOperation.GetDeferral();
                try
                {
                    this.ChangeScenarioState(ScenarioState.Idle);
                }
                finally
                {
                    deferral.Complete();
                }
            }
        }

        /// <summary>
        /// Initializes a new MediaCapture instance and starts the Preview streaming to the CamPreview UI element.
        /// </summary>
        /// <returns>Async Task object returning true if initialization and streaming were successful and false if an exception occurred.</returns>
        private async Task<bool> StartWebcamStreaming()
        {
            bool successful = true;

            try
            {
                this.mediaCapture = new MediaCapture();
                this.CameraPreviewManager = new CameraPreviewManager();
                var preferredDevice = await this.CameraPreviewManager.GetFilteredCameraOrDefaultAsync(await this.GetDeviceNameSelected());

                // For this scenario, we only need Video (not microphone) so specify this in the initializer.
                // NOTE: the appxmanifest only declares "webcam" under capabilities and if this is changed to include
                // microphone (default constructor) you must add "microphone" to the manifest or initialization will fail.
                MediaCaptureInitializationSettings settings = new MediaCaptureInitializationSettings
                {
                    StreamingCaptureMode = StreamingCaptureMode.Video,
                    VideoDeviceId = preferredDevice.Id
                };
                
                await this.mediaCapture.InitializeAsync(settings);
                this.mediaCapture.Failed += this.MediaCapture_CameraStreamFailed;

                var mediaEncondingProperties = this.DevicesSingleton.GetDeviceResolutionSelected(await GetDevicePropertieSelected());
                //var resolutions = mediaCapture.VideoDeviceController.GetAvailableMediaStreamProperties(MediaStreamType.VideoPreview).ToList();
                await mediaCapture.VideoDeviceController.SetMediaStreamPropertiesAsync(MediaStreamType.VideoPreview, mediaEncondingProperties);

                // Cache the media properties as we'll need them later.
                var deviceController = this.mediaCapture.VideoDeviceController;
                this.videoProperties = deviceController.GetMediaStreamProperties(MediaStreamType.VideoPreview) as VideoEncodingProperties;

                // Immediately start streaming to our CaptureElement UI.
                // NOTE: CaptureElement's Source must be set before streaming is started.
                this.CamPreview.Source = this.mediaCapture;
                await this.mediaCapture.StartPreviewAsync();

                // Use a 66 millisecond interval for our timer, i.e. 15 frames per second
                TimeSpan timerInterval = TimeSpan.FromMilliseconds(GetTimeSpanInterval());
                this.frameProcessingTimer = ThreadPoolTimer.CreatePeriodicTimer(new TimerElapsedHandler(ProcessCurrentVideoFrame), timerInterval);
            }
            catch (UnauthorizedAccessException)
            {
                // If the user has disabled their webcam this exception is thrown; provide a descriptive message to inform the user of this fact.
                this.rootPage.NotifyUser("Webcam is disabled or access to the webcam is disabled for this app.\nEnsure Privacy Settings allow webcam usage.", NotifyType.ErrorMessage);
                successful = false;
            }
            catch (Exception ex)
            {
                if (ex.ToString().ToUpper().Trim() != "A TASK WAS CANCELED")
                    this.rootPage.NotifyUser(ex.ToString(), NotifyType.ErrorMessage);

                successful = false;
            }

            return successful;
        }

        private int GetTimeSpanInterval()
        {
            object intervalParameter = this.UtilsSingleton.GetParameterFromDataStore(Constants.CONFIGURATION_TIMER_FACE_DETECT);
            int.TryParse(intervalParameter == null ? string.Empty : intervalParameter.ToString(), out int intervalValue);
            return intervalValue <= 0 ? 100 : intervalValue; //100 means min value.
        }

        /// <summary>
        /// Safely stops webcam streaming (if running) and releases MediaCapture object.
        /// </summary>
        private async Task ShutdownWebCam()
        {
            if (this.frameProcessingTimer != null)
            {
                this.frameProcessingTimer.Cancel();
            }

            if (this.mediaCapture != null)
            {
                if (this.mediaCapture.CameraStreamState == Windows.Media.Devices.CameraStreamState.Streaming)
                {
                    try
                    {
                        await this.mediaCapture.StopPreviewAsync();
                    }
                    catch (Exception)
                    {
                        ;   // Since we're going to destroy the MediaCapture object there's nothing to do here
                    }
                }
                this.mediaCapture.Dispose();
            }

            this.frameProcessingTimer = null;
            this.CamPreview.Source = null;
            this.mediaCapture = null;
            this.CameraStreamingButton.IsEnabled = true;
        }

        /// <summary>
        /// This method is invoked by a ThreadPoolTimer to execute the FaceTracker and Visualization logic at approximately 15 frames per second.
        /// </summary>
        /// <remarks>
        /// Keep in mind this method is called from a Timer and not synchronized with the camera stream. Also, the processing time of FaceTracker
        /// will vary depending on the size of each frame and the number of faces being tracked. That is, a large image with several tracked faces may
        /// take longer to process.
        /// </remarks>
        /// <param name="timer">Timer object invoking this call</param>
        private async void ProcessCurrentVideoFrame(ThreadPoolTimer timer)
        {
            if (this.currentState != ScenarioState.Streaming)
                return;
            //Protection from change page, put application on idle or does others proccess.
            if (this.mediaCapture is null || this.mediaCapture?.CameraStreamState == Windows.Media.Devices.CameraStreamState.NotStreaming
                || this.mediaCapture?.CameraStreamState == Windows.Media.Devices.CameraStreamState.Shutdown)
                return;

            // If a lock is being held it means we're still waiting for processing work on the previous frame to complete.
            // In this situation, don't wait on the semaphore but exit immediately.
            if (!frameProcessingSemaphore.Wait(0))
                return;

            try
            {
                IList<DetectedFace> faces = null;

                // Create a VideoFrame object specifying the pixel format we want our capture image to be (NV12 bitmap in this case).
                // GetPreviewFrame will convert the native webcam frame into this format.
                const BitmapPixelFormat InputPixelFormat = BitmapPixelFormat.Nv12;
                using (VideoFrame previewFrame = new VideoFrame(InputPixelFormat, (int)this.videoProperties.Width, (int)this.videoProperties.Height))
                {
                    await this.mediaCapture.GetPreviewFrameAsync(previewFrame);

                    // The returned VideoFrame should be in the supported NV12 format but we need to verify this.
                    if (FaceDetector.IsBitmapPixelFormatSupported(previewFrame.SoftwareBitmap.BitmapPixelFormat))
                    {
                        faces = await this.faceTracker.ProcessNextFrameAsync(previewFrame);
                    }
                    else
                    {
                        throw new System.NotSupportedException("PixelFormat '" + InputPixelFormat.ToString() + "' is not supported by FaceDetector");
                    }

                    // Create our visualization using the frame dimensions and face results but run it on the UI thread.
                    var previewFrameSize = new Windows.Foundation.Size(previewFrame.SoftwareBitmap.PixelWidth, previewFrame.SoftwareBitmap.PixelHeight);
                    var ignored = this.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        this.SetupVisualization(previewFrameSize, faces);
                    });
                }
            }
            catch (Exception ex)
            {
                var ignored = this.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    if(ex.ToString().ToUpper().Trim() != "A TASK WAS CANCELED")
                        this.rootPage.NotifyUser(ex.ToString(), NotifyType.ErrorMessage);
                });
            }
            finally
            {
                frameProcessingSemaphore.Release();
            }
        }

        protected override async void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            await ShutdownWebCam();
            base.OnNavigatingFrom(e);
        }

        /// <summary>
        /// Takes the webcam image and FaceTracker results and assembles the visualization onto the Canvas.
        /// </summary>
        /// <param name="framePizelSize">Width and height (in pixels) of the video capture frame</param>
        /// <param name="foundFaces">List of detected faces; output from FaceTracker</param>
        private void SetupVisualization(Windows.Foundation.Size framePizelSize, IList<DetectedFace> foundFaces)
        {
            this.VisualizationCanvas.Children.Clear();

            double actualWidth = this.VisualizationCanvas.ActualWidth;
            double actualHeight = this.VisualizationCanvas.ActualHeight;

            if (this.currentState == ScenarioState.Streaming && foundFaces != null && actualWidth != 0 && actualHeight != 0)
            {
                double widthScale = framePizelSize.Width / actualWidth;
                double heightScale = framePizelSize.Height / actualHeight;

                foreach (DetectedFace face in foundFaces)
                {
                    // Create a rectangle element for displaying the face box but since we're using a Canvas
                    // we must scale the rectangles according to the frames's actual size.
                    Rectangle box = new Rectangle();
                    box.Width = (uint)(face.FaceBox.Width / widthScale);
                    box.Height = (uint)(face.FaceBox.Height / heightScale);
                    box.Fill = this.fillBrush;
                    box.Stroke = this.lineBrush;
                    box.StrokeThickness = this.lineThickness;
                    box.Margin = new Thickness((uint)(face.FaceBox.X / widthScale), (uint)(face.FaceBox.Y / heightScale), 0, 0);

                    this.VisualizationCanvas.Children.Add(box);

                    if (foundFaces.Count <= 1) //Start kairos login only if has one face in front of the camera.
                        if (CompareFacesTracking(box)) //Process position of user to get best shot before try log in Kairos.
                            CallKairosLogin();
                }
            }
        }

        private int CountFramesToShoot()
        {
            object intervalParameter = this.UtilsSingleton.GetParameterFromDataStore(Constants.CONFIGURATION_COUNT_FRAMES_TO_SHOOT);
            int.TryParse(intervalParameter == null ? string.Empty : intervalParameter.ToString(), out int count);
            return count <= 0 ? 15 : count; //15 means min value.
        }

        private bool CompareFacesTracking(Rectangle box)
        {
            bool stabledFace = true;

            //Before take the best shoot from user, check if he is stabled.
            if(Boxes.Count <= CountFramesToShoot()) 
            {
                Boxes.Add(box);
                return false;
            }

            foreach(Rectangle oldBox in Boxes)
            {
                if (!stabledFace)
                    break;

                if (oldBox.Margin != box.Margin)
                    stabledFace = false;
            }

            this.Boxes.Clear();

            return stabledFace;
        }

        private async void CallKairosLogin()
        {
            //this.ChangeScenarioState(ScenarioState.Idle);
            await Dispatcher.RunAsync(CoreDispatcherPriority.High, () => 
            {
                CamPreview.Visibility = Visibility.Collapsed;
                VisualizationCanvas.Visibility = Visibility.Collapsed;
                Loading.Visibility = Visibility.Visible;
            });

            this.CameraFaceDetection = new CameraFaceDetection(this.mediaCapture);
            var inputBitmap = await this.CameraFaceDetection.CapturePhotoToByteArray();
            var base64 = this.UtilsSingleton.GetBase64FromByteArray(inputBitmap);
            var login = this.KairosSingleton.FaceLogin(base64);
            ShowLoginResult(login);
        }

        private async void ShowLoginResult(object result)
        {
            var authLogin = JsonConvert.DeserializeObject<AuthLogin>(result.ToString());
            this.rootPage.NotifyUser(authLogin.Message, authLogin.Success ? NotifyType.StatusMessage : NotifyType.ErrorMessage);

            if (authLogin.Success)
            {
                await ShutdownWebCam();
                this.KairosUserLogin = authLogin.SubjectID;
                this.Frame.Navigate(typeof(WebBrowserPage), this);
            }
            else
            {
                //this.ChangeScenarioState(ScenarioState.Streaming);
                await Dispatcher.RunAsync(CoreDispatcherPriority.High, () => 
                {
                    CamPreview.Visibility = Visibility.Visible;
                    VisualizationCanvas.Visibility = Visibility.Visible;
                    Loading.Visibility = Visibility.Collapsed;
                });
            }
        }

        /// <summary>
        /// Manages the scenario's internal state. Invokes the internal methods and updates the UI according to the
        /// passed in state value. Handles failures and resets the state if necessary.
        /// </summary>
        /// <param name="newState">State to switch to</param>
        private async void ChangeScenarioState(ScenarioState newState)
        {
            // Disable UI while state change is in progress
            this.CameraStreamingButton.IsEnabled = false;

            switch (newState)
            {
                case ScenarioState.Idle:

                    await this.ShutdownWebCam();

                    this.VisualizationCanvas.Children.Clear();
                    this.CameraStreamingButton.Content = "Start Preview";
                    this.currentState = newState;
                    break;

                case ScenarioState.Streaming:

                    if (!await this.StartWebcamStreaming())
                    {
                        this.ChangeScenarioState(ScenarioState.Idle);
                        break;
                    }

                    this.VisualizationCanvas.Children.Clear();
                    this.CameraStreamingButton.Content = "Stop Preview";
                    this.currentState = newState;
                    this.CameraStreamingButton.IsEnabled = true;
                    break;
            }
        }

        /// <summary>
        /// Handles MediaCapture stream failures by shutting down streaming and returning to Idle state.
        /// </summary>
        /// <param name="sender">The source of the event, i.e. our MediaCapture object</param>
        /// <param name="args">Event data</param>
        private void MediaCapture_CameraStreamFailed(MediaCapture sender, object args)
        {
            // MediaCapture is not Agile and so we cannot invoke its methods on this caller's thread
            // and instead need to schedule the state change on the UI thread.
            var ignored = this.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                ChangeScenarioState(ScenarioState.Idle);
            });
        }

        /// <summary>
        /// Handles "streaming" button clicks to start/stop webcam streaming.
        /// </summary>
        /// <param name="sender">Button user clicked</param>
        /// <param name="e">Event data</param>
        private void CameraStreamingButton_Click(object sender, RoutedEventArgs e)
        {
            EnterFullScreenCameraPreview();
            StartStreamingProcess();
        }

        private void EnterFullScreenCameraPreview()
        {
            if (this.CameraStreamingButton.Content.ToString() == "Start Preview")
            {
                this.stackPanelControls.Visibility = Visibility.Collapsed;
                this.VisualizationCanvas.Height = this.Frame.ActualHeight;
                this.VisualizationCanvas.Width = this.Frame.ActualWidth;
            }
        }

        private void StartStreamingProcess()
        {
            if (this.currentState == ScenarioState.Streaming)
            {
                this.rootPage.NotifyUser(string.Empty, NotifyType.StatusMessage);
                this.ChangeScenarioState(ScenarioState.Idle);
            }
            else
            {
                this.rootPage.NotifyUser(string.Empty, NotifyType.StatusMessage);
                this.ChangeScenarioState(ScenarioState.Streaming);
            }
        }

        void CoreWindow_CharacterReceived(CoreWindow sender, CharacterReceivedEventArgs args)
        {
            //Flag that keypress has been handled (to stop it repeating forever)
            args.Handled = true;

            //SHIFT + S -- Active streaming.
            if(args.KeyCode == 'S')
            {
                EnterFullScreenCameraPreview();
                StartStreamingProcess();
                return;
            }
            // ESC - Return screen UI elements.
            if (args.KeyCode != 27)
            {
                return;
            }
        }

        private async Task GetDevicesList()
        {
            var devices = await this.DevicesSingleton.GetDevices();
            foreach (DeviceInformation device in devices)
            {
                comboBoxSource.Items.Add(device.Name);
            }
        }

        private async Task GetDeviceProfiles(DeviceInformation device)
        {
            var deviceProfiles = await this.DevicesSingleton.GetDeviceProfiles(device);

            foreach (var item in deviceProfiles)
            {
                comboBoxProperties.Items.Add(item.FormatVideoEncodingProperties);
            }
        }

        private async Task<string> GetDeviceNameSelected()
        {
            if (comboBoxSource.Items.Count <= 0)
                await GetDevicesList();

            if (comboBoxSource.SelectedValue is null)
                comboBoxSource.SelectedIndex = 0;

            return comboBoxSource.SelectedValue.ToString();
        }

        private async Task<string> GetDevicePropertieSelected()
        {
            if (comboBoxProperties.Items.Count <= 0)
                await GetDeviceProfiles(this.DevicesSingleton.DeviceInformation);

            if (comboBoxProperties.SelectedValue is null)
                comboBoxProperties.SelectedIndex = 0;

            return comboBoxProperties.SelectedValue.ToString();
        }

        private async void ComboBoxSource_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBoxSource.SelectedIndex < 0)
                return;

            var device = this.DevicesSingleton.GetDeviceInformation(((ComboBox)sender).SelectedValue.ToString());

            await GetDeviceProfiles(device);
        }
    }
}
