﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Media.Capture;
using Windows.Media.MediaProperties;

namespace Bookshelf.Managers
{
    public class DevicesSingleton
    {
        private DevicesSingleton() {}
        private DeviceInformationCollection CameraDevices;
        public DeviceInformation DeviceInformation;
        private List<ExtendedVideoEncodingProperties> ExtendedVideoEncodingProperties;
        public ExtendedVideoEncodingProperties DeviceSelected;
        private static DevicesSingleton _DevicesSingleton;
        public static DevicesSingleton Instance
        {
            get
            {
                if (_DevicesSingleton is null)
                    _DevicesSingleton = new DevicesSingleton();

                return _DevicesSingleton;
            }
        }

        public IMediaEncodingProperties GetDeviceBestResolution(IReadOnlyList<IMediaEncodingProperties> resolutions)
        {
            //Starting filter the list.
            List<VideoEncodingProperties> propertiesList = new List<VideoEncodingProperties>();
            foreach (VideoEncodingProperties item in resolutions)
            {
                propertiesList.Add(item);
            }

            //Ordering by the best resolution the camera supports and return.
            var bestResolution = propertiesList.OrderByDescending(x => x.Width).OrderByDescending(x => x.Height).FirstOrDefault();
            return bestResolution != null ? bestResolution as IMediaEncodingProperties : resolutions[resolutions.Count - 1];
        }

        public IMediaEncodingProperties GetDeviceResolutionSelected(string deviceResolutionSelected)
        {
            if(this.DeviceSelected is null)
            this.DeviceSelected = this.ExtendedVideoEncodingProperties
                .FirstOrDefault(dev => dev.FormatVideoEncodingProperties.Equals(deviceResolutionSelected));

            return this.DeviceSelected.VideoProperties as IMediaEncodingProperties;
        }

        public async Task<DeviceInformationCollection> GetDevices()
        {
            if(this.CameraDevices is null || this.CameraDevices.Count == 0)
                this.CameraDevices = await DeviceInformation.FindAllAsync(DeviceClass.VideoCapture);

            return this.CameraDevices;
        }

        public async Task<IList<ExtendedVideoEncodingProperties>> GetDeviceProfiles(DeviceInformation device)
        {
            this.DeviceInformation = device;
            MediaCapture mediaCapture = new MediaCapture();

            MediaCaptureInitializationSettings settings = new MediaCaptureInitializationSettings
            {
                StreamingCaptureMode = StreamingCaptureMode.Video,
                VideoDeviceId = device.Id
            };

            await mediaCapture.InitializeAsync(settings);
            var profiles = mediaCapture.VideoDeviceController.GetAvailableMediaStreamProperties(MediaStreamType.VideoPreview).ToList();

            //Free from memory
            mediaCapture.Dispose();
            mediaCapture = null;

            FillExtendedVideoEncodingProperties(profiles);
            OrderExtendedVideoEncodingProperties();

            return this.ExtendedVideoEncodingProperties;
        }

        private void FillExtendedVideoEncodingProperties(IReadOnlyList<IMediaEncodingProperties> profiles)
        {
            this.ExtendedVideoEncodingProperties = new List<ExtendedVideoEncodingProperties>();

            foreach(VideoEncodingProperties item in profiles)
            {
                this.ExtendedVideoEncodingProperties.Add(
                    new Managers.ExtendedVideoEncodingProperties
                    {
                        VideoProperties = item,
                        FormatVideoEncodingProperties = $"{item.Width} x {item.Height} : {item.FrameRate.Numerator} / {item.Bitrate} (Width x Height : Frame Rate / Bitrate)"
                    });
            }
        }

        private void OrderExtendedVideoEncodingProperties()
        {
            this.ExtendedVideoEncodingProperties = this.ExtendedVideoEncodingProperties
                .OrderBy(dev => dev.VideoProperties.Width)
                .ThenBy(dev => dev.VideoProperties.Height)
                .ThenBy(dev => dev.VideoProperties.FrameRate.Numerator).ToList<ExtendedVideoEncodingProperties>();
        }

        public DeviceInformation GetDeviceInformation(string deviceName)
        {
            foreach(var device in this.CameraDevices)
            {
                if (device.Name.Equals(deviceName))
                    return device;
            }

            return null;
        }
    }

    public class ExtendedVideoEncodingProperties
    {
        public string FormatVideoEncodingProperties { get; set; }
        public VideoEncodingProperties VideoProperties { get; set; }
    }
}
