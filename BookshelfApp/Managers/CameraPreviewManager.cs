﻿using System.Linq;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;

namespace Bookshelf.Managers
{
    public class CameraPreviewManager
    {
        readonly DevicesSingleton DevicesSingleton;

        public CameraPreviewManager()
        {
            this.DevicesSingleton = DevicesSingleton.Instance;
        }
        
        public async Task<DeviceInformation> GetFilteredCameraOrDefaultAsync(string deviceFilter)
        {
            var videoCaptureDevices = await this.DevicesSingleton.GetDevices();

            var selectedCamera = videoCaptureDevices.SingleOrDefault(x=> x.Name.Trim().ToUpper().Equals(deviceFilter.Trim().ToUpper()));

            if (selectedCamera == null)
                selectedCamera = videoCaptureDevices.First();
            
            return (selectedCamera);
        }
    }
}