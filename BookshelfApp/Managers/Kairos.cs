﻿using System.Linq;
using System.Text;
using Kairos.Net;
using Newtonsoft.Json;

namespace Bookshelf.Managers
{
    public class Kairos
    {
        private Utils UtilsSingleton;
        private static KairosClient KairosClient;
        private Kairos()
        {
            this.UtilsSingleton = Utils.Instance;
            CreateKairosClientInstance();
        }

        private static Kairos _Kairos;
        public static Kairos Instance
        {
            get
            {
                if (_Kairos is null)
                {
                    _Kairos = new Kairos();
                }

                return _Kairos;
            }
        }

        private void CreateKairosClientInstance()
        {
            KairosClient = new KairosClient
            {
                ApplicationID = GetParameterFromCache(Constants.CONFIGURATION_KAIROS_APPLICATION_ID),
                ApplicationKey = GetParameterFromCache(Constants.CONFIGURATION_KAIROS_APPLICATION_KEY)
            };
        }

        private string GetParameterFromCache(string parameterName)
        {
            object parameter = this.UtilsSingleton.GetParameterFromDataStore(parameterName);
            return parameter is null ? string.Empty : parameter.ToString();
        }

        public object FaceLogin(string base64)
        {
            object result;
            // Recognize Face
            var recognizeResponse = KairosClient.Recognize(base64, GetParameterFromCache(Constants.CONFIGURATION_KAIROS_GALLERY_KEY));

            // Error handling
            if (recognizeResponse.Errors.Any())
            {
                var msg = new StringBuilder();
                foreach (var error in recognizeResponse.Errors)
                    msg.AppendLine($"{error.Message} ({error.ErrCode})");

                result = new { Success = false, Message = msg.ToString(), faceError = true };
                return JsonConvert.SerializeObject(result);
            }

            // Face recognized?
            var images = recognizeResponse.Images.FirstOrDefault();
            var isFaceRecognized = images?.Transaction != null && images.Transaction.status == "success";
            if (!isFaceRecognized)
            {
                result = new { Success = false, Message = Constants.KAIROS_FACE_NOT_RECOGNIZED_MESSAGE };
                return JsonConvert.SerializeObject(result);
            }
            // Will identify contact in Sitecore
            var subjectId = images.Transaction.subject_id;

            // Return Success
            result = new { Success = (subjectId != null) ? true : false, Message = (subjectId != null) ? "Success" : "False", SubjectID = subjectId};
            return JsonConvert.SerializeObject(result);
        }
    }
}
