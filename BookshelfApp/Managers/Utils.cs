﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Graphics.Display;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;

namespace Bookshelf.Managers
{
    public class Utils
    {
        private Utils() { }
        private static Utils _Utils;
        public static Utils Instance
        {
            get
            {
                if (_Utils is null)
                    _Utils = new Utils();

                return _Utils;
            }
        }

        public Size ScreenSize
        {
            get => GetScreenResolutionInfo();
        }

        private ApplicationDataContainer LocalSettings
        {
            get => ApplicationData.Current.LocalSettings;
        }

        private ApplicationDataCompositeValue CompositeValue
        {
            get => (ApplicationDataCompositeValue)LocalSettings.Values[Constants.UTILS_PARAMETERS_KEY];
        }

        private IDictionary<string, object> _StoredKeyValue;

        public IDictionary<string, object> StoredKeyValue
        {
            get
            {
                if (_StoredKeyValue is null)
                    _StoredKeyValue = CompositeValue;

                return _StoredKeyValue;
            }
            set => SetParameters(value);
        }

        private Size GetScreenResolutionInfo()
        {
            var applicationView = ApplicationView.GetForCurrentView();
            var displayInformation = DisplayInformation.GetForCurrentView();
            var bounds = applicationView.VisibleBounds;
            var scale = Convert.ToInt32(displayInformation.RawPixelsPerViewPixel);
            var size = new Size(Convert.ToInt32(bounds.Width) * scale, Convert.ToInt32(bounds.Height) * scale);
            return size;
        }

        private void SetParameters(IDictionary<string, object> keyValuePairs)
        {
            if (this._StoredKeyValue != null)
                this._StoredKeyValue.Clear();
            else
                this._StoredKeyValue = new ApplicationDataCompositeValue();

            object value;
            foreach (string key in keyValuePairs.Keys)
            {
                keyValuePairs.TryGetValue(key, out value);
                if (value != null)
                {
                    this._StoredKeyValue[key] = value;
                    value = null;
                }
            }

            this.LocalSettings.Values[Constants.UTILS_PARAMETERS_KEY] = this._StoredKeyValue;
        }

        public object GetParameterFromDataStore(string key)
        {
            return this._StoredKeyValue[key];
        }

        public async Task<string> GetBase64FromStorageFile(StorageFile input)
        {
            using (IRandomAccessStream memoryStream = await input.OpenAsync(FileAccessMode.ReadWrite))
            {
                await memoryStream.FlushAsync();
                memoryStream.Seek(0);

                byte[] array = new byte[memoryStream.Size];
                await memoryStream.ReadAsync(array.AsBuffer(), (uint)memoryStream.Size, InputStreamOptions.None);

                return GetBase64FromByteArray(array);
            }
        }

        public string GetBase64FromByteArray(byte[] array)
        {
            var base64String = Convert.ToBase64String(array);
            return base64String;
        }

        public bool UseFaceDetection(SoftwareBitmap softwareBitmap)
        {
            return
                (softwareBitmap?.PixelHeight > Window.Current.CoreWindow.Bounds.Height ||
                    softwareBitmap?.PixelWidth > Window.Current.Bounds.Width) ?
                false :
                true;
        }

        public async Task<StorageFile> OpenFileDialog_GetLocation()
        {
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            openPicker.FileTypeFilter.Add(".jpg");
            openPicker.FileTypeFilter.Add(".jpeg");
            openPicker.FileTypeFilter.Add(".png");

            return await openPicker.PickSingleFileAsync();
        }

        public async Task<string> GetFileContent(string fileName, Uri baseUri)
        {
            if (string.IsNullOrEmpty(fileName))
                return string.Empty;

            string fileContent = string.Empty;

            var file = await StorageFile.GetFileFromApplicationUriAsync(new Uri(baseUri, fileName));
            using (var inputStream = await file.OpenReadAsync())
            using (var classicStream = inputStream.AsStreamForRead())
            using (var streamReader = new StreamReader(classicStream))
            {
                while (streamReader.Peek() >= 0)
                {
                    fileContent += streamReader.ReadLine();
                }
            }

            return fileContent;
        }
    }

    public enum NotifyType
    {
        StatusMessage,
        ErrorMessage
    };

    public enum ScenarioState
    {
        /// <summary>
        /// Display is blank - default state.
        /// </summary>
        Idle,

        /// <summary>
        /// Webcam is actively engaged and a live video stream is displayed.
        /// </summary>
        Streaming
    }

    public class AuthLogin
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string SubjectID { get; set; }
    }


    public class SitecoreJsonResponse
    {
        [JsonProperty("sitecore")]
        public Sitecore Sitecore { get; set; }
    }

    public class Sitecore
    {
        [JsonProperty("route")]
        public Route Route { get; set; }
    }

    public class Route
    {
        [JsonProperty("placeholders")]
        public PlaceholdersCompositeBlock PlaceholdersCompositeBlock { get; set; }
    }

    public class PlaceholdersCompositeBlock
    {
        [JsonProperty("composite-block")]
        public CompositeBlockSingleColumnContainer[] CompositeBlockSingleColumnContainer { get; set; }
    }

    public class CompositeBlockSingleColumnContainer
    {
        [JsonProperty("placeholders")]
        public PlaceholdersSingleColumnContainer PlaceholdersSingleColumnContainer { get; set; }
    }

    public class PlaceholdersSingleColumnContainer
    {
        [JsonProperty("gridcolumn")]
        public GridcolumnSingleColumnContainer[] GridcolumnSingleColumnContainer { get; set; }
    }

    public class GridcolumnSingleColumnContainer
    {
        [JsonProperty("placeholders")]
        public PlaceholdersGenreBooksContinuous PlaceholdersGenreBooksContinuous { get; set; }
    }

    public class PlaceholdersGenreBooksContinuous
    {
        [JsonProperty("gridcolumn")]
        public GridcolumnGenreBooksContinuous[] GridcolumnGenreBooksContinuous { get; set; }
    }

    public class GridcolumnGenreBooksContinuous
    {
        [JsonProperty("fields")]
        public Fields Fields { get; set; }
    }

    public class Fields
    {
        [JsonProperty("Pin Number")]
        public PinNumber PinNumber { get; set; }

        [JsonProperty("Page Title")]
        public PageTitle PageTitle { get; set; }
    }

    public class PinNumber
    {
        [JsonProperty("value")]
        public string Pin { get; set; }
    }

    public class PageTitle
    {
        [JsonProperty("value")]
        public string Title { get; set; }
    }

}
