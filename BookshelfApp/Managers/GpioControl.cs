﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Gpio;

namespace Bookshelf.Managers
{
    public class GpioControl
    {
        private readonly int PinNumber;
        readonly GpioPin Pin;
        public GpioControl(int pin)
        {
            this.PinNumber = pin;
            var controller = GpioController.GetDefault();
            if (controller != null)
            {
                this.Pin = controller.OpenPin(this.PinNumber);
                this.Pin.SetDriveMode(GpioPinDriveMode.Output);
            }
        }

        public bool SendToPin(bool active)
        {
            if (this.Pin == null)
                return false;
            if (active)
                this.Pin.Write(GpioPinValue.Low);
            else
            {
                //When desactived, call dispose.
                this.Pin.Write(GpioPinValue.High);
                this.Pin.Dispose();
            }

            return true;
        }
    }
}
