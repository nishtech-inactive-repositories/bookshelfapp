﻿namespace Bookshelf.Managers
{
    public class Constants
    {
        public const string UTILS_PARAMETERS_KEY = "BookShelf_Keys";

        public const string CONFIGURATION_COUNT_FRAMES_TO_SHOOT = "COUNT_FRAMES_TO_SHOOT";
        public const string CONFIGURATION_COUNT_FRAMES_TO_SHOOT_MESSAGE = "Check the Count Frames to Shoot value, we expect a number.";
        public const string CONFIGURATION_TIMER_FACE_DETECT = "TIMER_FACE_DETECT";
        public const string CONFIGURATION_TIMER_FACE_DETECT_MESSAGE = "Check the Timer Face Detect value, we expect a number.";
        public const string CONFIGURATION_TIMER_EXPERIENCE = "TIMER_EXPERIENCE";
        public const string CONFIGURATION_TIMER_EXPERIENCE_MESSAGE = "Check the Timer Experience value, we expect a number.";
        public const string CONFIGURATION_KAIROS_APPLICATION_ID = "KAIROS_ID";
        public const string CONFIGURATION_KAIROS_APPLICATION_ID_MESSAGE = "Check the Kairos ID value, it can't be empty.";
        public const string CONFIGURATION_KAIROS_APPLICATION_KEY = "KAIROS_KEY";
        public const string CONFIGURATION_KAIROS_APPLICATION_KEY_MESSAGE = "Check the Kairos Key value, it can't be empty.";
        public const string CONFIGURATION_KAIROS_GALLERY_KEY = "KAIROS_GALLERY_KEY";
        public const string CONFIGURATION_KAIROS_GALLERY_KEY_MESSAGE = "Check the Kairos Gallery ID value, it can't be empty.";
        public const string CONFIGURATION_SITECORE_URL = "SITECORE_URL";
        public const string CONFIGURATION_SITECORE_URL_MESSAGE = "Check the Sitecore URL value, it can't be empty.";
        public const string CONFIGURATION_SITECORE_URL_API = "SITECORE_URL_API";
        public const string CONFIGURATION_SITECORE_URL_API_MESSAGE = "Check the Sitecore URL API value, it can't be empty.";
        public const string CONFIGURATION_IDENTIFIER_NAME = "IDENTIFIER_NAME";
        public const string CONFIGURATION_IDENTIFIER_NAME_MESSAGE = "Check the Identifier Name value, it can't be empty.";

        public const string KAIROS_FACE_NOT_RECOGNIZED_MESSAGE = "Your face has not been recognized";

        public const string BOOKSHELF_MASTER_FEATURE_NAME = "BookShelf";

        public const string WEBBROWSER_HEADER_NAME = "IdentifierName";
        public const string WEBBROWSER_HEADER_VALUE = "SUGCON"; //TO-DO
        public const string WEBBROWSER_HEADER_KAIROS = "IdentifierValue";
        public const string WEBBROWSER_MESSAGE_OK = "Check the Leds.";
        public const string WEBBROWSER_MESSAGE_NO_SECTIONS = "No sections were found.";
        public const string WEBBROWSER_ERROR_MESSAGE_SITECORE_RESULTS = "Something was wrong in getting Sections.";

    }
}
