﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Media.Capture;
using Windows.Media.FaceAnalysis;
using Windows.Media.MediaProperties;
using Windows.Storage;
using Windows.Storage.Streams;

namespace Bookshelf.Managers
{
    public class CameraFaceDetection
    {
        private FaceDetector FaceDetector;
        private BitmapPixelFormat FaceDetectorSupportedPixelFormat;
        private MediaCapture CameraMediaCapture { get; set; }
        private CameraPreviewManager CameraPreviewManager { get; set; }
        public CameraFaceDetection() { }
        public CameraFaceDetection(MediaCapture mediaCapture)
        {
            this.CameraMediaCapture = mediaCapture;
        }

        public async Task<SoftwareBitmap> GetSoftwareBitmapFromStorageFile(StorageFile input)
        {
            using (IRandomAccessStream stream = await input.OpenAsync(FileAccessMode.Read))
            {
                // Create the decoder from the stream
                var decoder = await BitmapDecoder.CreateAsync(stream);

                // Get the SoftwareBitmap representation of the file
                var softwareBitmap = await decoder.GetSoftwareBitmapAsync();

                return SoftwareBitmap.Convert(softwareBitmap, BitmapPixelFormat.Bgra8, BitmapAlphaMode.Premultiplied);
            }
        }

        public async Task<byte[]> CapturePhotoToByteArray()
        {
            var imageEncodingProperties = ImageEncodingProperties.CreateJpeg();

            var memoryStream = new InMemoryRandomAccessStream();
            try
            {
                await this.CameraMediaCapture.CapturePhotoToStreamAsync(imageEncodingProperties, memoryStream);
            }
            catch (Exception ex)
            {
                Debug.Print(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }

            await memoryStream.FlushAsync();
            memoryStream.Seek(0);

            byte[] array = new byte[memoryStream.Size];
            await memoryStream.ReadAsync(array.AsBuffer(), (uint)memoryStream.Size, Windows.Storage.Streams.InputStreamOptions.None);
            return array;
        }

        public async Task InitializeFaceDetection()
        {
            if (!FaceDetector.IsSupported)
                return;
            if (this.FaceDetector != null)
                return;

            this.FaceDetector = await FaceDetector.CreateAsync();
            FaceDetectorSupportedPixelFormat = FaceDetector.GetSupportedBitmapPixelFormats().FirstOrDefault();
        }

        public async Task<IList<DetectedFace>> DetectFaces(SoftwareBitmap inputBitmap)
        {
            if (FaceDetector is null)
                return null;

            if (!FaceDetector.IsBitmapPixelFormatSupported(inputBitmap.BitmapPixelFormat))
                inputBitmap = SoftwareBitmap.Convert(inputBitmap, FaceDetectorSupportedPixelFormat);

            return await FaceDetector.DetectFacesAsync(inputBitmap);
        }
    }
}
