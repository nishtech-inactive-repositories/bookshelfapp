﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;

namespace Bookshelf
{
    public class Scenario
    {
        public string Title { get; set; }
        public Type ClassType { get; set; }
    }
    public partial class MainPage : Page
    {
        List<Scenario> scenarios = new List<Scenario>
        {
            new Scenario { Title="Configurations", ClassType=typeof(ConfigurationsPage) },
            new Scenario { Title="Manual Face Detection", ClassType=typeof(ManualFaceDetectionPage) },
            new Scenario { Title="Face Detection", ClassType=typeof(FaceDetection) },
            new Scenario {Title="Gpio Test", ClassType=typeof(GpioTestPage)}
        };
    }
}
